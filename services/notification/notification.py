import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from pydantic import BaseModel

EMAIL_LOGIN = "info@blacklink.ru"
EMAIL_PASSWORD = "blacklinkru"
SMTP = "smtp.yandex.ru"


class Notification(BaseModel):
    url: str
    message_text: str

    def send(self):
        raise NotImplementedError


class EmailNotification(Notification):
    mail: str

    def send(self):
        message = MIMEMultipart()
        message["From"] = EMAIL_LOGIN
        message["To"] = self.mail
        message["Subject"] = "Resource unavailable."
        message.attach(MIMEText(self.message_text, "plain", "utf-8"))
        server = smtplib.SMTP(SMTP)
        server.starttls()
        server.login(EMAIL_LOGIN, EMAIL_PASSWORD)
        server.sendmail(message["From"], message["To"], message.as_string())
        server.quit()


class SMSNotification(Notification):
    phone: str

    def send(self):
        pass


class TelegramNotification(Notification):
    client_id: int

    def send(self):
        pass
