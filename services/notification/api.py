from fastapi import FastAPI

from .notification import EmailNotification
from .notification import SMSNotification
from .notification import TelegramNotification

app = FastAPI(title="Notification")


@app.get(
    "/",
    response_description="",
    description=""
)
async def index():

    return {"name": "notification service"}


@app.post(
    "/send/email/",
    response_description="",
    description="",
    status_code=201
)
async def send_email_notification(email_notification: EmailNotification):
    email_notification.send()
    return {"status": "OK"}


@app.post(
    "/send/sms/",
    response_description="",
    description="",
    status_code=201
)
async def send_sms_notification(sms_notification: SMSNotification):
    sms_notification.send()
    return {"status": "OK"}


@app.post(
    "/send/telegram/",
    response_description="",
    description="",
    status_code=201
)
async def send_telegram_notification(telegram_notification: TelegramNotification):
    telegram_notification.send()
    return {"status": "OK"}
