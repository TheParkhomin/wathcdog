from fastapi import FastAPI
from fastapi import HTTPException

app = FastAPI(title="Random phrase")


@app.get(
    "/",
    response_description="",
    description=""
)
async def index():

    return {"name": "web service"}
