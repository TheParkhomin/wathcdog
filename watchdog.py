import asyncio

import aiohttp
import yaml


async def process_error(session, **kwargs):
    message_text = (
        f"Resource {kwargs.get('url')} unavailable, "
        f"status error = {kwargs.get('status_code')}"
    )
    notification_email_url = "http://127.0.0.1:8000/send/email/"
    data = {
        "url": kwargs.get("url"),
        "mail": kwargs.get("mail"),
        "message_text": message_text
    }

    async with session.post(notification_email_url, json=data) as response:
        if response.status != 201:
            pass


async def check_site(
        mail: str, url: str, interval: int, session: aiohttp.ClientSession
):
    while True:
        async with session.get(url) as response:
            if response.status != 200:
                await process_error(
                    session,
                    mail=mail,
                    url=url,
                    status_code=response.status,
                )
            await asyncio.sleep(interval)


async def watchdog(config_path: str):
    interval_seconds = 10  # it is interval in seconds between requests.
    with open(config_path) as sites_yaml:
        sites = yaml.safe_load(sites_yaml)

    async with aiohttp.ClientSession() as session:
        tasks = []
        for site in sites:
            for mail in sites[site]:
                tasks.append(
                    asyncio.create_task(
                        check_site(mail, site, interval_seconds, session)
                    )
                )
        await asyncio.wait(tasks)


def main():
    default_sites_config_path = "sites.yaml"
    event_loop = asyncio.get_event_loop()
    event_loop.run_until_complete(watchdog(default_sites_config_path))
    event_loop.close()


if __name__ == "__main__":
    main()
