# watchdog

## Installation

```
$ pip install -r requirements.txt
```


## Run

```
$ uvicorn services.notification.api:app --reload
$ python watchdog.py
```
